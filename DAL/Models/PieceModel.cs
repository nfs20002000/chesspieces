﻿namespace DAL.Models
{
    public class PieceModel
    {
        public int Id { get; set; }

        public string Abbreviation { get; set; }

        public string Name { get; set; }

        public string MoveList { get; set; }

        public PieceModel(int id, string abbreviation, string name, string moveList)
        {
            Id = id;
            Abbreviation = abbreviation;
            Name = name;
            MoveList = moveList;
        }
    }
}
