﻿using DAL.Models;

namespace DAL.Interfaces
{
    public interface IPieceRepository
    {
        ICollection<PieceModel>? GetPieces();

        PieceModel? GetPiece(int id);

        PieceModel InsertPiece(PieceModel piece);

        PieceModel? UpdatePiece(PieceModel piece);

        int DeletePiece(int id);
    }
}
