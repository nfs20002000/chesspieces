﻿using DAL.Interfaces;
using DAL.Models;
using Microsoft.Data.SqlClient;
using System.Data;

namespace DAL.Repositories
{
    public class PieceRepository : IPieceRepository
    {
        private const string connectionString = "Server=LAPTOP-UH95G4SR\\SQLSERVER2013;Database=ChessPieces;Trusted_Connection=True;Encrypt=False;";

        public DataRow? FindPiece(DataTable dt, int id)
        {
            foreach (DataRow row in dt.Rows)
            {
                var cells = row.ItemArray;
                if ((int)row.ItemArray[0]! == id)
                {
                    return row;
                }
            }
            return null;
        }

        public int DeletePiece(int id)
        {
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                var sqlQuery = $"SELECT * FROM Piece WHERE Id = {id}";
                SqlDataAdapter dataAdapter = new SqlDataAdapter(sqlQuery, connection);
                DataSet dataSet = new DataSet();
                dataAdapter.Fill(dataSet);

                if (dataSet.Tables[0].Rows.Count != 0)
                {
                    dataSet.Tables[0].Rows[0].Delete();
                    SqlCommandBuilder commandBuilder = new SqlCommandBuilder(dataAdapter);
                    dataAdapter.Update(dataSet);
                    return 0;
                }
            }

            return 1;
        }

        public PieceModel? GetPiece(int id)
        {
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                SqlDataAdapter adapter = new SqlDataAdapter("SELECT * FROM Piece", connection);
                DataSet ds = new DataSet();
                adapter.Fill(ds);
                foreach (DataTable dt in ds.Tables)
                {
                    var result = FindPiece(dt, id);
                    if (result != null)
                    {
                        return new PieceModel
                            (
                                (int)result.ItemArray[0]!,
                                (string)result.ItemArray[1]!,
                                (string)result.ItemArray[2]!,
                                (string)result.ItemArray[3]!
                            );
                    }
                }
            }

            return null;
        }

        public ICollection<PieceModel>? GetPieces()
        {
            List<PieceModel> pieces = new List<PieceModel>();

            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                SqlDataAdapter adapter = new SqlDataAdapter("SELECT * FROM Piece", connection);
                DataSet ds = new DataSet();
                adapter.Fill(ds);

                if (ds.Tables.Count > 0)
                {
                    DataTable dt = ds.Tables[0];

                    foreach (DataRow row in dt.Rows)
                    {
                        var cells = row.ItemArray;
                        PieceModel piece = new PieceModel((int)cells[0]!, (string)cells[1]!, (string)cells[2]!, (string)cells[3]!);
                        pieces.Add(piece);
                    }

                    return pieces;
                }
            }

            return null;
        }

        public PieceModel InsertPiece(PieceModel piece)
        {
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                var sqlQuery = "SELECT * FROM Piece WHERE 0 = 1";
                SqlDataAdapter dataAdapter = new SqlDataAdapter(sqlQuery, connection);
                DataSet dataSet = new DataSet();
                dataAdapter.Fill(dataSet);

                var newRow = dataSet.Tables[0].NewRow();
                newRow["Abbreviation"] = piece.Abbreviation;
                newRow["Name"] = piece.Name;
                newRow["MoveList"] = piece.MoveList;
                dataSet.Tables[0].Rows.Add(newRow);

                SqlCommandBuilder commandBuilder = new SqlCommandBuilder(dataAdapter);
                dataAdapter.Update(dataSet);
            }

            return piece;
        }

        public PieceModel? UpdatePiece(PieceModel piece)
        {
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                var sqlQuery = $"SELECT * FROM Piece WHERE Id = {piece.Id}";
                SqlDataAdapter dataAdapter = new SqlDataAdapter(sqlQuery, connection);
                DataSet dataSet = new DataSet();
                dataAdapter.Fill(dataSet);

                if (dataSet.Tables[0].Rows.Count != 0)
                {
                    dataSet.Tables[0].Rows[0]["Abbreviation"] = piece.Abbreviation;
                    dataSet.Tables[0].Rows[0]["Name"] = piece.Name;
                    dataSet.Tables[0].Rows[0]["MoveList"] = piece.MoveList;
                    SqlCommandBuilder commandBuilder = new SqlCommandBuilder(dataAdapter);
                    dataAdapter.Update(dataSet);
                    return piece;
                }
            }

            return null;
        }
    }
}
