﻿namespace PresentationLayer
{
    public class Square : Button
    {
        public int hor, ver;
        public Square(int col, int hor, int ver)
        {
            this.hor = hor;
            this.ver = ver;
            if (col == 0)
            {
                this.BackColor = Color.White;
            }
            else
            {
                this.BackColor = Color.Gray;
            }
        }
    }
}
