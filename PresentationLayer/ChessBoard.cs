﻿using BLL.Interfaces;
using BLL.ViewModels;
using PresentationLayer.Helpers;
using PresentationLayer.Interfaces;

namespace PresentationLayer
{
    public class ChessBoard
    {
        public IPieceService pieceService { get; set; }

        public Square[,] Squares = new Square[8, 8];
        public Square PieceSquare { get; set; }
        public ChessBoard(IPieceService pieceService)
        {
            Init();
            this.pieceService = pieceService;
            PieceSquare = Squares[4, 3]; // Фигура всегда расположена на поле d4
            this.pieceService.GetPieces();
            ShowPiece(this.pieceService.Position);
        }

        public void Init()
        {
            for (int i = 0; i < 8; i++)
            {
                for (int j = 0; j < 8; j++)
                {
                    Squares[i, j] = new Square((i + j) % 2, i, j);
                    Squares[i, j].Size = new Size(50, 50);
                    Squares[i, j].Location = new Point(j * 50, i * 50);
                }
            }
        }

        public void CloseSteps()
        {
            for (int i = 0; i < 8; i++)
            {
                for (int j = 0; j < 8; j++)
                {
                    if ((i + j) % 2 == 0)
                    {
                        Squares[i, j].BackColor = Color.White;
                    }
                    else
                    {
                        Squares[i, j].BackColor = Color.Gray;
                    }
                }
            }
        }

        public void ShowPiece(int position)
        {
            CloseSteps();
            PieceViewModel piece = this.pieceService.Pieces[position];
            PieceSquare.Text = piece.Abbreviation;
            List<(int, int)> coordinates = CoordinatesConverter.ConvertIntoBoardCoordinates(piece.MoveListConverter());

            foreach (var coordinate in coordinates)
            {
                int vertical = coordinate.Item1;
                int horizontal = coordinate.Item2;
                Squares[vertical, horizontal].BackColor = Color.Red;
            }
        }

        public void MakeBoardInsertionMode()
        {
            CloseSteps();
            PieceSquare.Text = null;
            PieceSquare.BackColor = Color.Yellow;

            for (int i = 0; i < 8; i++)
            {
                for (int j = 0; j < 8; j++)
                {
                    Squares[i, j].Click -= OnSquarePress;
                    Squares[i, j].Click += OnSquarePress;
                }
            }
        }

        public void MakeBoardUpdateMode()
        {
            int position = this.pieceService.Position;
            this.ShowPiece(position);

            for (int i = 0; i < 8; i++)
            {
                for (int j = 0; j < 8; j++)
                {
                    Squares[i, j].Click -= OnSquarePress;
                    Squares[i, j].Click += OnSquarePress;
                }
            }

            PieceSquare.Click -= OnSquarePress;
        }

        public void RefreshBoard()
        {
            this.pieceService.GetPieces();
            this.pieceService.Position = 0;
            ShowPiece(this.pieceService.Position);
        }

        public void DeleteEvents()
        {
            for (int i = 0; i < 8; i++)
            {
                for (int j = 0; j < 8; j++)
                {
                    Squares[i, j].Click -= OnSquarePress;
                }
            }
        }

        public List<(int, int)> CollectSelectedSquares()
        {
            List<(int, int)> selectedSquares = new List<(int, int)>();

            for (int i = 0; i < 8; i++)
            {
                for (int j = 0; j < 8; j++)
                {
                    if (Squares[i, j].BackColor == Color.Red)
                    {
                        int horizontal = Squares[i, j].hor;
                        int vertical = Squares[i, j].ver;
                        selectedSquares.Add(new(horizontal, vertical));
                    }
                }
            }

            return selectedSquares;
        }

        private void OnSquarePress(object sender, EventArgs e)
        {
            Square square = sender as Square;

            if (square.BackColor == Color.White || square.BackColor == Color.Gray)
            {
                square.BackColor = Color.Red;
                return;
            }

            if (square.BackColor == Color.Red)
            {
                square.BackColor = (square.hor + square.ver) % 2 == 0 ? Color.White : Color.Gray;
                return;
            }
        }
    }
}
