﻿namespace PresentationLayer
{
    partial class Form1
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.previousPiece = new System.Windows.Forms.Button();
            this.nextPiece = new System.Windows.Forms.Button();
            this.refreshPiecesList = new System.Windows.Forms.Button();
            this.deletePiece = new System.Windows.Forms.Button();
            this.insertPiece = new System.Windows.Forms.Button();
            this.updatePiece = new System.Windows.Forms.Button();
            this.abbreviationLabel = new System.Windows.Forms.Label();
            this.nameLabel = new System.Windows.Forms.Label();
            this.abbreviationTextBox = new System.Windows.Forms.TextBox();
            this.nameTextBox = new System.Windows.Forms.TextBox();
            this.viewRadioButton = new System.Windows.Forms.RadioButton();
            this.insertRadioButton = new System.Windows.Forms.RadioButton();
            this.updateRadioButton = new System.Windows.Forms.RadioButton();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // previousPiece
            // 
            this.previousPiece.Enabled = false;
            this.previousPiece.Location = new System.Drawing.Point(484, 45);
            this.previousPiece.Name = "previousPiece";
            this.previousPiece.Size = new System.Drawing.Size(109, 48);
            this.previousPiece.TabIndex = 0;
            this.previousPiece.Text = "Предыдущая фигура";
            this.previousPiece.UseVisualStyleBackColor = true;
            this.previousPiece.Click += new System.EventHandler(this.previousPiece_Click);
            // 
            // nextPiece
            // 
            this.nextPiece.Location = new System.Drawing.Point(645, 45);
            this.nextPiece.Name = "nextPiece";
            this.nextPiece.Size = new System.Drawing.Size(123, 48);
            this.nextPiece.TabIndex = 1;
            this.nextPiece.Text = "Следующая фигура";
            this.nextPiece.UseVisualStyleBackColor = true;
            this.nextPiece.Click += new System.EventHandler(this.nextPiece_Click);
            // 
            // refreshPiecesList
            // 
            this.refreshPiecesList.Location = new System.Drawing.Point(484, 118);
            this.refreshPiecesList.Name = "refreshPiecesList";
            this.refreshPiecesList.Size = new System.Drawing.Size(109, 73);
            this.refreshPiecesList.TabIndex = 2;
            this.refreshPiecesList.Text = "Обновить список фигур";
            this.refreshPiecesList.UseVisualStyleBackColor = true;
            this.refreshPiecesList.Click += new System.EventHandler(this.refreshPiecesList_Click);
            // 
            // deletePiece
            // 
            this.deletePiece.Location = new System.Drawing.Point(645, 118);
            this.deletePiece.Name = "deletePiece";
            this.deletePiece.Size = new System.Drawing.Size(123, 73);
            this.deletePiece.TabIndex = 3;
            this.deletePiece.Text = "Удалить фигуру";
            this.deletePiece.UseVisualStyleBackColor = true;
            this.deletePiece.Click += new System.EventHandler(this.deletePiece_Click);
            // 
            // insertPiece
            // 
            this.insertPiece.Enabled = false;
            this.insertPiece.Location = new System.Drawing.Point(484, 229);
            this.insertPiece.Name = "insertPiece";
            this.insertPiece.Size = new System.Drawing.Size(109, 57);
            this.insertPiece.TabIndex = 4;
            this.insertPiece.Text = "Добавить фигуру";
            this.insertPiece.UseVisualStyleBackColor = true;
            this.insertPiece.Click += new System.EventHandler(this.insertPiece_Click);
            // 
            // updatePiece
            // 
            this.updatePiece.Enabled = false;
            this.updatePiece.Location = new System.Drawing.Point(645, 229);
            this.updatePiece.Name = "updatePiece";
            this.updatePiece.Size = new System.Drawing.Size(123, 57);
            this.updatePiece.TabIndex = 5;
            this.updatePiece.Text = "Редактировать фигуру";
            this.updatePiece.UseVisualStyleBackColor = true;
            this.updatePiece.Click += new System.EventHandler(this.updatePiece_Click);
            // 
            // abbreviationLabel
            // 
            this.abbreviationLabel.AutoSize = true;
            this.abbreviationLabel.Location = new System.Drawing.Point(485, 321);
            this.abbreviationLabel.Name = "abbreviationLabel";
            this.abbreviationLabel.Size = new System.Drawing.Size(109, 20);
            this.abbreviationLabel.TabIndex = 6;
            this.abbreviationLabel.Text = "Аббревиатура";
            // 
            // nameLabel
            // 
            this.nameLabel.AutoSize = true;
            this.nameLabel.Location = new System.Drawing.Point(670, 321);
            this.nameLabel.Name = "nameLabel";
            this.nameLabel.Size = new System.Drawing.Size(77, 20);
            this.nameLabel.TabIndex = 7;
            this.nameLabel.Text = "Название";
            // 
            // abbreviationTextBox
            // 
            this.abbreviationTextBox.Enabled = false;
            this.abbreviationTextBox.Location = new System.Drawing.Point(484, 375);
            this.abbreviationTextBox.MaxLength = 2;
            this.abbreviationTextBox.Name = "abbreviationTextBox";
            this.abbreviationTextBox.Size = new System.Drawing.Size(125, 27);
            this.abbreviationTextBox.TabIndex = 8;
            // 
            // nameTextBox
            // 
            this.nameTextBox.Enabled = false;
            this.nameTextBox.Location = new System.Drawing.Point(645, 375);
            this.nameTextBox.Name = "nameTextBox";
            this.nameTextBox.Size = new System.Drawing.Size(125, 27);
            this.nameTextBox.TabIndex = 9;
            // 
            // viewRadioButton
            // 
            this.viewRadioButton.AutoSize = true;
            this.viewRadioButton.Checked = true;
            this.viewRadioButton.Location = new System.Drawing.Point(58, 445);
            this.viewRadioButton.Name = "viewRadioButton";
            this.viewRadioButton.Size = new System.Drawing.Size(101, 24);
            this.viewRadioButton.TabIndex = 11;
            this.viewRadioButton.TabStop = true;
            this.viewRadioButton.Text = "Просмотр";
            this.viewRadioButton.UseVisualStyleBackColor = true;
            this.viewRadioButton.CheckedChanged += new System.EventHandler(this.viewRadioButton_CheckedChanged);
            // 
            // insertRadioButton
            // 
            this.insertRadioButton.AutoSize = true;
            this.insertRadioButton.Location = new System.Drawing.Point(230, 445);
            this.insertRadioButton.Name = "insertRadioButton";
            this.insertRadioButton.Size = new System.Drawing.Size(116, 24);
            this.insertRadioButton.TabIndex = 12;
            this.insertRadioButton.Text = "Добавление";
            this.insertRadioButton.UseVisualStyleBackColor = true;
            this.insertRadioButton.CheckedChanged += new System.EventHandler(this.insertRadioButton_CheckedChanged);
            // 
            // updateRadioButton
            // 
            this.updateRadioButton.AutoSize = true;
            this.updateRadioButton.Location = new System.Drawing.Point(381, 445);
            this.updateRadioButton.Name = "updateRadioButton";
            this.updateRadioButton.Size = new System.Drawing.Size(144, 24);
            this.updateRadioButton.TabIndex = 13;
            this.updateRadioButton.Text = "Редактирование";
            this.updateRadioButton.UseVisualStyleBackColor = true;
            this.updateRadioButton.CheckedChanged += new System.EventHandler(this.updateRadioButton_CheckedChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(18, 406);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(19, 20);
            this.label1.TabIndex = 14;
            this.label1.Text = "A";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(65, 406);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(18, 20);
            this.label2.TabIndex = 15;
            this.label2.Text = "B";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(114, 406);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(18, 20);
            this.label3.TabIndex = 16;
            this.label3.Text = "C";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(165, 406);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(20, 20);
            this.label4.TabIndex = 17;
            this.label4.Text = "D";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(213, 406);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(17, 20);
            this.label5.TabIndex = 18;
            this.label5.Text = "E";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(267, 406);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(16, 20);
            this.label6.TabIndex = 19;
            this.label6.Text = "F";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(314, 406);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(19, 20);
            this.label7.TabIndex = 20;
            this.label7.Text = "G";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(363, 406);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(20, 20);
            this.label8.TabIndex = 21;
            this.label8.Text = "H";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(401, 367);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(17, 20);
            this.label9.TabIndex = 22;
            this.label9.Text = "1";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(401, 313);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(17, 20);
            this.label10.TabIndex = 23;
            this.label10.Text = "2";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(401, 258);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(17, 20);
            this.label11.TabIndex = 24;
            this.label11.Text = "3";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(401, 208);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(17, 20);
            this.label12.TabIndex = 25;
            this.label12.Text = "4";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(401, 163);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(17, 20);
            this.label13.TabIndex = 26;
            this.label13.Text = "5";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(401, 110);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(17, 20);
            this.label14.TabIndex = 27;
            this.label14.Text = "6";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(401, 65);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(17, 20);
            this.label15.TabIndex = 28;
            this.label15.Text = "7";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(401, 21);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(17, 20);
            this.label16.TabIndex = 29;
            this.label16.Text = "8";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(890, 515);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.updateRadioButton);
            this.Controls.Add(this.insertRadioButton);
            this.Controls.Add(this.viewRadioButton);
            this.Controls.Add(this.nameTextBox);
            this.Controls.Add(this.abbreviationTextBox);
            this.Controls.Add(this.nameLabel);
            this.Controls.Add(this.abbreviationLabel);
            this.Controls.Add(this.updatePiece);
            this.Controls.Add(this.insertPiece);
            this.Controls.Add(this.deletePiece);
            this.Controls.Add(this.refreshPiecesList);
            this.Controls.Add(this.nextPiece);
            this.Controls.Add(this.previousPiece);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Button previousPiece;
        private Button nextPiece;
        private Button refreshPiecesList;
        private Button deletePiece;
        private Button insertPiece;
        private Button updatePiece;
        private Label abbreviationLabel;
        private Label nameLabel;
        private TextBox abbreviationTextBox;
        private TextBox nameTextBox;
        private RadioButton viewRadioButton;
        private RadioButton insertRadioButton;
        private RadioButton updateRadioButton;
        private Label label1;
        private Label label2;
        private Label label3;
        private Label label4;
        private Label label5;
        private Label label6;
        private Label label7;
        private Label label8;
        private Label label9;
        private Label label10;
        private Label label11;
        private Label label12;
        private Label label13;
        private Label label14;
        private Label label15;
        private Label label16;
    }
}