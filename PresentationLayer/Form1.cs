using BLL.Services;
using BLL.ViewModels;
using DAL.Repositories;
using Microsoft.IdentityModel.Tokens;
using PresentationLayer.Helpers;
using PresentationLayer.Interfaces;

namespace PresentationLayer
{
    public partial class Form1 : Form
    {
        private ILogger logger;

        public ChessBoard Board = new ChessBoard(new PieceService(new PieceRepository()));
        public Form1(ILogger logger)
        {
            InitializeComponent();
            this.logger = logger;

            for (int i = 0; i < 8; i++)
            {
                for (int j = 0; j < 8; j++)
                {
                    this.Controls.Add(Board.Squares[i, j]);
                }
            }
        }

        public void Init()
        {
            for (int i = 0; i < 8; i++)
            {
                for (int j = 0; j < 8; j++)
                {
                    Board.Squares[i, j] = new Square((i + j) % 2, i, j);
                    Board.Squares[i, j].Size = new Size(50, 50);
                    Board.Squares[i, j].Location = new Point(j * 50, i * 50);
                }
            }
        }

        private void SwitchEnablability(bool onOff)
        {
            nameTextBox.Enabled = onOff;
            abbreviationTextBox.Enabled = onOff;
            refreshPiecesList.Enabled = !onOff;
            previousPiece.Enabled = !onOff;
            nextPiece.Enabled = !onOff;
            deletePiece.Enabled = !onOff;
        }

        private int TextFieldsCheck(string abbreviation, string name)
        {
            if (abbreviation.IsNullOrEmpty() || name.IsNullOrEmpty())
            {
                this.logger.Log("��� ��������� ���� ������ ���� ���������!");
                return 1;
            }

            int position = Board.pieceService.Position;

            if (Board.pieceService.Pieces.Any(x => x.Abbreviation == abbreviation) &&
                this.updateRadioButton.Checked == true &&
                Board.pieceService.Pieces[position].Abbreviation != abbreviation)
            {
                this.logger.Log("����� ������������ ��� ����! ����������, ������� ������ ������������.");
                return 1;
            }

            if (Board.pieceService.Pieces.Any(x => x.Name == name) &&
                this.updateRadioButton.Checked == true &&
                Board.pieceService.Pieces[position].Name != name)
            {
                this.logger.Log("����� ��� ������ ��� ����! ����������, ������� ������ ���.");
                return 1;
            }

            return 0;
        }

        public string PieceCoordinatesCollection()
        {
            List<(int, int)> selectedSquares = Board.CollectSelectedSquares();

            if (selectedSquares.Count == 0)
            {
                this.logger.Log("����� �� �������! ����������, �������� ���� �� ���� ����.");
                return null;
            }

            string coordinates = "";

            foreach (var coordinate in selectedSquares)
            {
                coordinates += CoordinatesConverter.ConvertNumbersCoordinateIntoString(coordinate);
                coordinates += " ";
            }

            coordinates = coordinates.Remove(coordinates.Length - 1);
            return coordinates;
        }

        public void SwitchToViewMode()
        {
            viewRadioButton.Checked = true;
            Board.DeleteEvents();
            Board.RefreshBoard();
        }

        #region Events
        private void refreshPiecesList_Click(object sender, EventArgs e)
        {
            Board.RefreshBoard();
            previousPiece.Enabled = false;
            nextPiece.Enabled = true;
        }

        private void previousPiece_Click(object sender, EventArgs e)
        {
            nextPiece.Enabled = true;
            Board.pieceService.Position--;
            int position = Board.pieceService.Position;
            Board.ShowPiece(position);

            if (position == 0)
            {
                previousPiece.Enabled = false;
            }
        }

        private void nextPiece_Click(object sender, EventArgs e)
        {
            previousPiece.Enabled = true;
            Board.pieceService.Position++;
            int position = Board.pieceService.Position;
            Board.ShowPiece(position);

            if (position == Board.pieceService.Pieces.Count - 1)
            {
                nextPiece.Enabled = false;
            }
        }

        private void deletePiece_Click(object sender, EventArgs e)
        {
            int position = Board.pieceService.Position;
            int pieceId = Board.pieceService.Pieces[position].Id;
            int result = Board.pieceService.DeletePiece(pieceId);

            if (result == 0)
            {
                this.logger.Log("������ ������� �������!");
            }
            else
            {
                this.logger.Log("�� ������� ������� ������...");
            }

            Board.RefreshBoard();
            previousPiece.Enabled = false;
            nextPiece.Enabled = true;
        }

        private void insertPiece_Click(object sender, EventArgs e)
        {
            string abbreviation = abbreviationTextBox.Text;
            string name = nameTextBox.Text;

            int check = TextFieldsCheck(abbreviation, name);
            if (check == 1)
            {
                return;
            }

            string coordinates = PieceCoordinatesCollection();

            PieceViewModel piece = new PieceViewModel(abbreviation, name, coordinates);

            piece = Board.pieceService.InsertPiece(piece);

            if (piece != null)
            {
                this.logger.Log("������ ������� �������!");
            }
            else
            {
                this.logger.Log("���-�� ����� �� ���... ������ �� �������.");
            }

            SwitchToViewMode();
        }

        private void updatePiece_Click(object sender, EventArgs e)
        {
            string abbreviation = abbreviationTextBox.Text;
            string name = nameTextBox.Text;

            int check = TextFieldsCheck(abbreviation, name);
            if (check == 1)
            {
                return;
            }

            string coordinates = PieceCoordinatesCollection();

            int id = Board.pieceService.Pieces[Board.pieceService.Position].Id;
            PieceViewModel piece = new PieceViewModel(id, abbreviation, name, coordinates);

            piece = Board.pieceService.UpdatePiece(piece);

            if (piece != null)
            {
                this.logger.Log("������ ������� ���������!");
            }
            else
            {
                this.logger.Log("���-�� ����� �� ���... ������ �� ���������.");
            }

            SwitchToViewMode();
        }

        private void viewRadioButton_CheckedChanged(object sender, EventArgs e)
        {
            if (viewRadioButton.Checked)
            {
                abbreviationTextBox.Text = null;
                nameTextBox.Text = null;
                SwitchEnablability(false);
                insertPiece.Enabled = false;
                updatePiece.Enabled = false;
                Board.DeleteEvents();
                Board.RefreshBoard();
                previousPiece.Enabled = false;
                nextPiece.Enabled = true;
            }
        }

        private void insertRadioButton_CheckedChanged(object sender, EventArgs e)
        {
            if (insertRadioButton.Checked)
            {
                abbreviationTextBox.Text = null;
                nameTextBox.Text = null;
                SwitchEnablability(true);
                insertPiece.Enabled = true;
                updatePiece.Enabled = false;
                Board.MakeBoardInsertionMode();
            }
        }

        private void updateRadioButton_CheckedChanged(object sender, EventArgs e)
        {
            if (updateRadioButton.Checked)
            {

                SwitchEnablability(true);

                insertPiece.Enabled = false;
                updatePiece.Enabled = true;

                Board.MakeBoardUpdateMode();

                int position = Board.pieceService.Position;
                abbreviationTextBox.Text = Board.pieceService.Pieces[position].Abbreviation;
                nameTextBox.Text = Board.pieceService.Pieces[position].Name;
            }
        }

        #endregion
    }
}