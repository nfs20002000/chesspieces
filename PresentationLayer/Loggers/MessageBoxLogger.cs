﻿using PresentationLayer.Interfaces;

namespace PresentationLayer.Loggers
{
    public class MessageBoxLogger : ILogger
    {
        public void Log(string message)
        {
            MessageBox.Show(message);
        }
    }
}
