﻿namespace PresentationLayer.Interfaces
{
    public interface ILogger
    {
        void Log(string message);
    }
}
