﻿namespace PresentationLayer.Helpers
{
    public static class CoordinatesConverter
    {
        public static string ConvertNumbersCoordinateIntoString((int, int) coordinate)
        {
            string convertedCoordinate = "";

            char letter = Convert.ToChar(coordinate.Item2 + 97);
            char number = Convert.ToChar(56 - coordinate.Item1);
            convertedCoordinate += letter;
            convertedCoordinate += number;

            return convertedCoordinate;
        }

        public static List<(int, int)> ConvertIntoBoardCoordinates(string[] coordinates)
        {
            List<(int, int)> boardCoordinates = new List<(int, int)>();

            foreach (var coordinate in coordinates)
            {
                (int, int) convertedCoordinate = ConvertStringCoordinateIntoNumbers(coordinate);
                boardCoordinates.Add(convertedCoordinate);
            }

            return boardCoordinates;

            (int, int) ConvertStringCoordinateIntoNumbers(string coordinate)
            {
                char symbol = Convert.ToChar(coordinate[0]);
                char lowerSymbol = char.ToLower(symbol);
                int vertical = Convert.ToInt32(lowerSymbol) - 97;
                char number = coordinate[1];
                int horizontal = 7 - (Convert.ToInt32(number) - 49);

                return new(horizontal, vertical);
            }
        }
    }
}
