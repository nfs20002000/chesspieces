﻿using BLL.ViewModels;

namespace BLL.Interfaces
{
    public interface IPieceService
    {
        List<PieceViewModel> Pieces { get; set; }

        int Position { get; set; }

        ICollection<PieceViewModel>? GetPieces();

        PieceViewModel GetPiece(int id);

        PieceViewModel InsertPiece(PieceViewModel piece);

        PieceViewModel? UpdatePiece(PieceViewModel piece);

        int DeletePiece(int id);
    }
}
