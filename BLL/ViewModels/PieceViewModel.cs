﻿namespace BLL.ViewModels
{
    public class PieceViewModel
    {
        public int Id { get; set; }

        public string Abbreviation { get; set; }

        public string Name { get; set; }

        public string MoveList { get; set; }

        public PieceViewModel(string abbreviation, string name, string moveList)
        {
            Abbreviation = abbreviation;
            Name = name;
            MoveList = moveList;
        }

        public PieceViewModel(int id, string abbreviation, string name, string moveList) : this(abbreviation, name, moveList)
        {
            Id = id;
        }

        public string[] MoveListConverter()
        {
            return MoveList.Split(' ');
        }
    }
}
