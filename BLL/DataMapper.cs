﻿using AutoMapper;
using BLL.ViewModels;
using DAL.Models;

namespace BLL
{
    public static class DataMapper
    {
        private static readonly ConfigurationStore ConfigurationStore;
        private static readonly MappingEngine MappingEngine;

        static DataMapper()
        {
            ConfigurationStore = new ConfigurationStore(new TypeMapFactory(), AutoMapper.Mappers.MapperRegistry.AllMappers())
            {
                AllowNullCollections = true,
                AllowNullDestinationValues = true,
            };
            ConfigurationStore.CreateMap<PieceModel, PieceViewModel>();
            ConfigurationStore.CreateMap<PieceViewModel, PieceModel>();
            MappingEngine = new MappingEngine(ConfigurationStore);
        }

        public static PieceModel ToModel(this PieceViewModel pieceViewModel)
        {
            return MappingEngine.Map<PieceModel>(pieceViewModel);
        }

        public static PieceViewModel ToViewModel(this PieceModel pieceModel)
        {
            return MappingEngine.Map<PieceViewModel>(pieceModel);
        }
    }
}
