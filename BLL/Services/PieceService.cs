﻿using BLL.Interfaces;
using BLL.ViewModels;
using DAL.Interfaces;

namespace BLL.Services
{
    public class PieceService : IPieceService
    {
        private readonly IPieceRepository pieceRepository;

        public List<PieceViewModel> Pieces { get; set; } = new List<PieceViewModel>();

        public int Position { get; set; } = 0;

        public PieceService(IPieceRepository pieceRepository)
        {
            this.pieceRepository = pieceRepository;
        }
        public int DeletePiece(int id)
        {
            return pieceRepository.DeletePiece(id);
        }

        public PieceViewModel GetPiece(int id)
        {
            return pieceRepository.GetPiece(id)!.ToViewModel();
        }

        public ICollection<PieceViewModel>? GetPieces()
        {
            Pieces = pieceRepository.GetPieces()!.Select(x => x.ToViewModel()).ToList();
            return Pieces;
        }

        public PieceViewModel InsertPiece(PieceViewModel piece)
        {
            return pieceRepository.InsertPiece(piece.ToModel()).ToViewModel();
        }

        public PieceViewModel? UpdatePiece(PieceViewModel piece)
        {
            return pieceRepository.UpdatePiece(piece.ToModel())!.ToViewModel();
        }
    }
}
